CBLandingPage = angular.module('CBLandingPage', [
        'CBLandingPage.models'
        'CBLandingPage.filters',
        'CBLandingPage.services',
        'CBLandingPage.directives',
        'CBLandingPage.controllers.app',
        'CBLandingPage.controllers.appInstance',
        'CBLandingPage.controllers.browser'
    ]
)
