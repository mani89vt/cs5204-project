Async = require('async')
{EventEmitter} = require('events')
cloudbrowserError = require('../../shared/cloudbrowser_error')

class AppInstance extends EventEmitter
    constructor : (@app, template, owner, @id, @name) ->
        @obj = template.create()
        @owner = owner
        @readerwriters = []
        @dateCreated = new Date()
        @browsers = []

    _findReaderWriter : (user) ->
        return c for c in @readerwriters when c.getEmail() is user.getEmail()

    getReaderWriters : () ->
        return @readerwriters

    getID : () -> return @id

    getName : () -> return @name

    getDateCreated : () -> return @dateCreated

    getOwner : () -> return @owner

    getObj : () -> return @obj

    isOwner : (user) ->
        return user.getEmail() is @owner.getEmail()

    isReaderWriter : (user) ->
        return true for c in @readerwriters when c.getEmail() is user.getEmail()

    addReaderWriter : (user) ->
        @readerwriters.push(user)
        @emit('share', user)

    save : (user) ->
        if @isOwner(user) or @isReaderWriter(user) then @template.save()

    createBrowser : (user, callback) ->

        if @isOwner user or @isReaderWriter user
            await @app.browsers.create user : user, preLoadMethod : (bserver) => bserver.setAppInstance(@)  defer bserver
            @browsers.push(bserver)
            callback null, bserver
        else callback(cloudbrowserErro('PERM_DENIED'))

        ###
        if @isOwner(user) or @isReaderWriter(user)
            Async.waterfall [
                (next) =>
                    @app.browsers.create
                        user     : user
                        callback : next
                        preLoadMethod : (bserver) => bserver.setAppInstance(@)
                (bserver, next) =>
                    @browsers.push(bserver)
                    next(null, bserver)
            ], callback
        else callback(cloudbrowserError('PERM_DENIED'))
        ###

    removeBrowser : (bserver, user, callback) ->

        {id} = bserver
        await @app.browsers.close bserver, user, defer callback
        await
            for browser in @browsers when browser.id is id
                idx = @browsers.indexOf browser
                @browsers.splice idx, 1
                break
            defer callback
        ###
        {id} = bserver
        Async.waterfall [
            (next) =>
                @app.browsers.close(bserver, user, next)
            (next) =>
                for browser in @browsers when browser.id is id
                    idx = @browsers.indexOf(browser)
                    @browsers.splice(idx, 1)
                    break
                next(null)
        ], callback
        ###

    removeAllBrowsers : (user, callback) ->

        if @isOwner user or @isReaderWriter user
            await
                for browser in @browsers
                    @app.browsers.close browser, user, defer callback
        else callback(cloudbrowserError('PERM_DENIED'))
        ###
        if @isOwner(user) or @isReaderWriter(user)
            Async.each @browsers
            , (browser, callback) =>
                @app.browsers.close(browser, user, callback)
            , callback
        else callback(cloudbrowserError('PERM_DENIED'))
        ###

    close : (user, callback) ->
        @removeAllListeners()
        @removeAllBrowsers(user, callback)

module.exports = AppInstance
